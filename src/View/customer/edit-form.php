<div class="modal fade" id="editCustomerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Editar Cliente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="selectedId" name="selectedId">
                <div class="form-floating">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nome do cliente">
                    <label for="name">Nome</label>
                </div>
                <div class="form-floating">
                    <input type="text" id="cpf" name="cpf" class="form-control" placeholder="CPF do cliente">
                    <label for="cpf">CPF</label>
                </div>
                <div class="form-floating">
                    <select class="form-select" id="stepSelect" aria-label="Etapa Atual">
                        <option value="1">Aguardando assinatura de documentos</option>
                        <option value="2">Aguardando transferência de recursos</option>
                        <option value="3">Gestão de patrimônio ativa</option>
                    </select>
                    <label for="stepSelect">Etapa Atual</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" onclick="saveCustomer()" class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</div>