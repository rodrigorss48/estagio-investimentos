<div class="modal fade" id="deleteCustomerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="deleteCustomerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteCustomerModalLabel">Excluir Cliente?</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
            </div>
            <div class="modal-body">
                <h3>Você tem certeza que deseja excluir este cliente?</h3>
                <input type="hidden" id="deleteId" name="deleteId">
                <div class="form-floating">
                    <input type="text" id="deleteName" name="name" class="form-control" placeholder="Nome do cliente" disabled>
                    <label for="deleteName">Nome</label>
                </div>
                <div class="form-floating">
                    <input type="text" id="deleteCpf" name="cpf" class="form-control" placeholder="CPF do cliente" disabled>
                    <label for="deleteCpf">CPF</label>
                </div>
                <div class="form-floating">
                    <select class="form-select" id="deleteStepSelect" aria-label="Etapa Atual" disabled>
                        <option value="1">Aguardando assinatura de documentos</option>
                        <option value="2">Aguardando transferência de recursos</option>
                        <option value="3">Gestão de patrimônio ativa</option>
                    </select>
                    <label for="deleteStepSelect">Etapa Atual</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" onclick="deleteCustomer()" class="btn btn-danger">Excluir</button>
            </div>
        </div>
    </div>
</div>