<div aria-live="polite" aria-atomic="true" class="position-relative">
    <div class="toast-container position-absolute top-0 start-50 translate-middle-x p-3" id="toasts">
        <div class="toast align-items-stretch text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true" id="successEditToast">
            <div class="d-flex">
                <div class="toast-body">
                    Cliente alterado com sucesso!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
        <div class="toast align-items-stretch text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true" id="errorEditToast">
            <div class="d-flex">
                <div class="toast-body">
                    Erro ao alterar cliente!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
        <div class="toast align-items-stretch text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true" id="successNewToast">
            <div class="d-flex">
                <div class="toast-body">
                    Cliente cadastrado com sucesso!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
        <div class="toast align-items-stretch text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true" id="errorNewToast">
            <div class="d-flex">
                <div class="toast-body">
                    Erro ao cadastrar cliente!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
        <div class="toast align-items-stretch text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true" id="successDeleteToast">
            <div class="d-flex">
                <div class="toast-body">
                    Cliente excluído com sucesso!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
        <div class="toast align-items-stretch text-white bg-danger border-0" role="alert" aria-live="assertive" aria-atomic="true" id="errorDeleteToast">
            <div class="d-flex">
                <div class="toast-body">
                    Erro ao excluir cliente!
                </div>
                <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast" aria-label="Fechar"></button>
            </div>
        </div>
    </div>
</div>