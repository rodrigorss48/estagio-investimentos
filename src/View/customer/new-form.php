<div class="modal fade" id="newCustomerModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="newCustomerLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newCustomerLabel">Cadastrar Cliente</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
            </div>
            <div class="modal-body">
                <div class="form-floating">
                    <input type="text" id="newName" name="name" class="form-control" placeholder="Nome do cliente">
                    <label for="newName">Nome</label>
                </div>
                <div class="form-floating">
                    <input type="text" id="newCpf" name="cpf" class="form-control" placeholder="CPF do cliente">
                    <label for="newCpf">CPF</label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" onclick="newCustomer()" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>
    </div>
</div>