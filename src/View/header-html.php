<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Rodrigo Silva">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <title><?= $title ?> | Desafio CDV</title>
</head>

<body>
    <div class="container">
        <div class="bg-light p-4 my-3 rounded-3">
            <h1 class="fw-bold text-center"><?= $title ?></h1>
        </div>