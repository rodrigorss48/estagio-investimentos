<?php

namespace CDV\Model;

final class OnboardingStep
{
    public const STEP_WAITING_DOCS = 1;
    public const STEP_WAITING_TRANSFER = 2;
    public const STEP_ACTIVE = 3;

    public static $choices = [
        self::STEP_WAITING_DOCS => "Aguardando assinatura de documentos",
        self::STEP_WAITING_TRANSFER => "Aguardando transferência de recursos",
        self::STEP_ACTIVE => "Gestão de patrimônio ativa"
    ];
}
