<?php

namespace CDV\Model;


/**
 * @Entity
 * @Table(name="customers")
 */
class Customer implements \JsonSerializable
{

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @Column(type="string",length=100)
     * @var string
     */
    private $name;

    /**
     * @Column(type="string",length=11)
     * @var string
     */
    private $CPF;

    /**
     * @Column(type="integer")
     * @var integer
     * Uses OnboardingStep
     */
    private $currentStep;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCPF(): string
    {
        return $this->CPF;
    }

    public function setCPF(string $CPF): void
    {
        $this->CPF = $CPF;
    }

    public function getCurrentStep(): int
    {
        return $this->currentStep;
    }

    /**
     * Gets the current step in text form
     */
    public function getCurrentStepText(): string
    {
        return OnboardingStep::$choices[$this->currentStep];
    }

    public function setCurrentStep(int $currentStep): void
    {
        $this->currentStep = $currentStep;
    }

    /** Allows the object to be serialized
     * while still having private members */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this)
            + ["currentStepText" => $this->getCurrentStepText()];

        return $vars;
    }
}
