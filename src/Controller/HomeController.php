<?php

namespace CDV\Controller;

use CDV\Controller\ControllerInterface;

class HomeController implements ControllerInterface
{


    public function handleRequest(): void
    {
        $title = "Clientes";
        include __DIR__ . '/../View/header-html.php'; ?>

        <div class="text-center">
            <button type="button" onclick="$('#newCustomerModal').modal('show');" class="btn btn-primary mb-2 mx-auto">
                Cadastrar cliente
            </button>
        </div>

        <ul class="list-group" id="customers">


        </ul>

        </div>

        <?php include __DIR__ . '/../View/customer/toasts.php' ?>
        <?php include __DIR__ . '/../View/customer/edit-form.php' ?>
        <?php include __DIR__ . '/../View/customer/new-form.php' ?>
        <?php include __DIR__ . '/../View/customer/delete-form.php' ?>
        <script type="text/javascript" src="/js/jquery-3.6.0.min.js"></script>
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/js/customers.js"></script>
<?php include __DIR__ . '/../View/footer-html.php';
    }
}
