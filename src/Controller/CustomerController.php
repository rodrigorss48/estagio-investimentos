<?php

namespace CDV\Controller;

use CDV\Model\Customer;
use CDV\Controller\ControllerInterface;
use CDV\Infra\EntityManagerCreator;
use CDV\Model\OnboardingStep;

class CustomerController implements ControllerInterface
{

    private $customerRepository;
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private $entityManager;

    public function __construct()
    {
        $this->entityManager = (new EntityManagerCreator())
            ->getEntityManager();
        $this->customerRepository = $this->entityManager
            ->getRepository(Customer::class);
    }

    public function handleRequest(): void
    {
        header("Content-Type: application/json; charset=UTF-8");
        switch ($_SERVER["REQUEST_METHOD"]) {
            case "GET":
                if (!isset($_GET["id"])) {
                    $result = $this->customerRepository->findAll();
                    echo json_encode($result);
                } else {
                    $userId = (int) $_GET["id"];
                    $result = $this->customerRepository->find($userId);
                    if (!$result) {
                        http_response_code(404);
                    } else {
                        echo json_encode($result);
                    }
                }
                break;
            case "PUT":
                $input = (array) json_decode(file_get_contents('php://input'), TRUE);
                if (!$this->validateCustomer($input)) {
                    http_response_code(422);
                } else {
                    $id = (int)filter_var($input['id'], FILTER_VALIDATE_INT);
                    $result = $this->customerRepository->find($id);
                    if (!$result) {
                        http_response_code(404);
                    } else {
                        $cust = new Customer();
                        $cust->setId($id);
                        $name = filter_var($input['name'], FILTER_SANITIZE_STRING);
                        $cust->setName($name);
                        $cpf = filter_var($input['CPF'], FILTER_SANITIZE_STRING);
                        $cust->setCPF($cpf);
                        $currentStep = (int)filter_var($input['currentStep'], FILTER_VALIDATE_INT);
                        $cust->setCurrentStep($currentStep);
                        $this->entityManager->merge($cust);
                        $this->entityManager->flush();
                        echo "{}";
                        http_response_code(200);
                    }
                }
                break;
            case "POST":
                $input = (array) json_decode(file_get_contents('php://input'), TRUE);

                $name = filter_var($input['name'], FILTER_SANITIZE_STRING);
                $cpf = filter_var($input['CPF'], FILTER_SANITIZE_STRING);
                if (!$name || !$cpf) {
                    http_response_code(422);
                } else {
                    $cust = new Customer();
                    $cust->setName($name);
                    $cust->setCPF($cpf);
                    $cust->setCurrentStep(OnboardingStep::STEP_WAITING_DOCS);
                    $this->entityManager->persist($cust);
                    $this->entityManager->flush();
                    echo "{}";
                    http_response_code(200);
                }
                break;
            case "DELETE":
                $input = (array) json_decode(file_get_contents('php://input'), TRUE);
                $id = (int)filter_var($input['id'], FILTER_VALIDATE_INT);
                $result = $this->customerRepository->find($id);
                if (!$result) {
                    http_response_code(404);
                } else {
                    $cust = $this->entityManager->getReference(
                        Customer::class,
                        $id
                    );
                    $this->entityManager->remove($cust);
                    $this->entityManager->flush();
                    echo "{}";
                    http_response_code(200);
                }
                break;
            default:
                http_response_code(400);
        }
    }

    private function validateCustomer($input)
    {
        if (!isset($input['id'])) {
            return false;
        }
        if (!isset($input['name'])) {
            return false;
        }
        if (!isset($input['CPF'])) {
            return false;
        }
        if (!isset($input['currentStep']) || (int)$input['currentStep'] > 3 || (int)$input['currentStep'] < 1) {
            return false;
        }
        return true;
    }
}
