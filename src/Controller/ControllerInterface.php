<?php

namespace CDV\Controller;

interface ControllerInterface
{
    public function handleRequest(): void;
}
