$(document).ready(function() {
    loadCustomers();
});
function loadCustomers() {
    $.ajax({
        url: "/api/customers"
    }).then(function(data) {
        if (data.length > 0) {
            $('#customers').html("");
            data.forEach(cust => {
                $('#customers').append(`
                <li id="cust-${cust.id}" class="list-group-item d-flex justify-content-around">
                    <span class="col-md-auto my-auto fw-bold text-center">${cust.name}</span>
                    <span class="col-2 my-auto text-center d-none d-md-block">${cust.CPF}</span>
                    <span class="col-lg-auto my-auto fw-bold text-center d-none d-lg-block ${cust.currentStep === 3 ? "text-success" : "text-danger"}">${cust.currentStepText}</span>

                    <span class="col-md-auto my-auto text-center">
                        <a href="#" onclick="editCustomer(${cust.id})" class="btn btn-info btn-sm">
                            Alterar
                        </a>
                        <a href="#" onclick="confirmDelete(${cust.id})" class="btn btn-danger btn-sm">
                            Excluir
                        </a>
                    </span>
                </li>
                `);
            });
        } else {
            $('#customers').html('<div class="alert alert-danger" role="alert">Nenhum cliente encontrado.</div>');
        }
    });
}
function editCustomer(id){
    $.ajax({
        url: "/api/customers?id="+id
    }).then(function(cust) {
        $('#selectedId').val(cust.id);
        $('#name').val(cust.name);
        $('#cpf').val(cust.CPF);
        $('#stepSelect').val(cust.currentStep);
        $('#editCustomerModal').modal('show');
    });
}
function saveCustomer(){
    var cust = {
        id: Number($('#selectedId').val()),
        name: $('#name').val(),
        CPF: $('#cpf').val(),
        currentStep: Number($('#stepSelect').val())
    }
    $.ajax({
        url: "/api/customers",
        type: "PUT",
        data: JSON.stringify(cust),
        headers: {
            'Content-Type': 'application/json'
        },
        success: function(result) {
            loadCustomers();
            $('#editCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#successEditToast"));
            toast.show();
        },
        error: function(result) {
            $('#editCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#errorEditToast"));
            toast.show();
        }
    });
}
function newCustomer(){
    var cust = {
        name: $('#newName').val(),
        CPF: $('#newCpf').val(),
    }
    $.ajax({
        url: "/api/customers",
        type: "POST",
        data: JSON.stringify(cust),
        headers: {
            'Content-Type': 'application/json'
        },
        success: function(result) {
            loadCustomers();
            $('#newCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#successNewToast"));
            toast.show();
        },
        error: function(result) {
            $('#newCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#errorNewToast"));
            toast.show();
        }
    });
}
function confirmDelete(id){
    $.ajax({
        url: "/api/customers?id="+id
    }).then(function(cust) {
        $('#deleteId').val(cust.id);
        $('#deleteName').val(cust.name);
        $('#deleteCpf').val(cust.CPF);
        $('#deleteStepSelect').val(cust.currentStep);
        $('#deleteCustomerModal').modal('show');
    });
}
function deleteCustomer(){
    var cust = {
        id: Number($('#deleteId').val()),
        name: $('#deleteName').val(),
        CPF: $('#deleteCpf').val(),
        currentStep: Number($('#deleteStepSelect').val())
    }
    $.ajax({
        url: "/api/customers",
        type: "DELETE",
        data: JSON.stringify(cust),
        headers: {
            'Content-Type': 'application/json'
        },
        success: function(result) {
            loadCustomers();
            $('#deleteCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#successDeleteToast"));
            toast.show();
        },
        error: function(result) {
            $('#deleteCustomerModal').modal('hide');
            var toast = new bootstrap.Toast($("#errorDeleteToast"));
            toast.show();
        }
    });
}