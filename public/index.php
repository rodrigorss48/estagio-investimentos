<?php

require __DIR__ . '/../vendor/autoload.php';

use CDV\Controller\ControllerInterface;

// get current path or default to home
$currPath = $_SERVER['PATH_INFO'] ?? "/home";
$routing = require __DIR__ . '/../config/routing.php';

// return a 404 if specified route is not mapped
if (!array_key_exists($currPath, $routing)) {
    http_response_code(404);
    exit();
}

$controllerClass = $routing[$currPath];

/** @var ControllerInterface $controller */
$controller = new $controllerClass();
$controller->handleRequest();
