<?php

use CDV\Controller\{HomeController, CustomerController};

return [
    '/home' => HomeController::class,
    '/api/customers' => CustomerController::class
];
